FROM node:slim

# copy soure code into container
COPY . /app
WORKDIR /app

RUN npm install
#stuctured like this bc want to run two commands
CMD echo "It's the second TP and I'm still lost" && npm start
EXPOSE 3000